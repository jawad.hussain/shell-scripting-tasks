#!/bin/bash

echo "Enter data you want to find"
read data
echo -e "String searched: $data \nSearching from directory: Harry_Potter\n" > output.txt
grep -inr $data Harry_Potter >> output.txt # not case sensitive - prints line number too
