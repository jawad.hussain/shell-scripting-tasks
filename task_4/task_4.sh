#!/bin/bash

loc=~/Backup 			   #backup path
loc2=$1

[ ! -d $loc ] && mkdir $loc        #create backup dir if it does not exist

cd $loc2

for file_name in *.c;			   #get every .c file in directory
do
       if [ -f $loc/$file_name ]; then

		if ! cmp -s $file_name $loc/$file_name ; then

			cp -p $file_name $loc/$file_name
			echo "File $file_name has been Updated"
		else
			echo "File $file_name is Up-to-Date"
		fi
        else
                echo "File $file_name had no previous backup copy. Backup has been created. "
		cp -p $file_name $loc/$file_name
fi
done

