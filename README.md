# READ ME

## Task1:  
	Command:  ./task_1  
	Discription: Find word in file  
	Process: Enter word to search for when prompted  
	Output: In output.txt file  
	Note: nevigate to correct folder first  
	
## Task2:  
	Command:  ./task_2 <Rows> <Columns>  
	Discription: Output certain row and column from csv file  
	Process: nil  
	Output: On terminal  
	Note: nevigate to correct folder first  
	  
## Task3:  
	Command:  ./task_3 <file location>  
	Discription: Sort files according to size  
	Process: nil  
	Output: On termintal  
	Note: nevigate to correct folder first  
	  
## Task4:  
	Command:  ./task_4 <location of files to backup>  
	Discription: Create Backup and update it  
	Process: nil  
	Output: In Backup folder in home dir. Also on terminal  
	Note: nevigate to correct folder first  
  	
## Task5:  
	Command:  ./task_5  
	Discription: Print log file name that has run time less then 9.5 hrs  
	Process: nil  
	Output: On terminal  
	Note: nevigate to correct folder first  
  	
## Task6:  
	Command:  ./task_6  
	Discription: extract data and sort file then find mean, median, sum, min, max value  
	Process: nil  
	Output: On terminal  
	Note: nevigate to correct folder first  
