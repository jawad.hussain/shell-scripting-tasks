#!/bin//bash


loc=./logs				   #log path
end_time=9.5
cd $loc


for file_name in *.log;			   #get every log file in directory
do
	data="$(grep -oP 'run-time-hours \K.*' $file_name)"
	data="$(printf '%.2f \n' $data)"
	if (( $(echo "$data < $end_time" | bc -l) )); then
		echo "File $file_name ran for $data hrs"
	fi
done
