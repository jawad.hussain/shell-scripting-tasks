#!/bin/bash
file=raw_data_for_stats_calculation.txt
cd data
cat $file | awk '{print $(NF)}' > non_sorted_data.txt
cat non_sorted_data.txt | sort > sorted_data.txt

sum=$(awk '{ sum += $1 } END { print sum }' sorted_data.txt)
lines=$(wc -l < sorted_data.txt)
med=$(($lines/2))
echo "Median: $( awk -v a=$med 'NR==a {print}' sorted_data.txt )"
echo "Mean: $(( $sum / $lines )) "
echo "Sum: $sum"
echo "Minimum: $( head -n 1 sorted_data.txt )"
echo "Maximum: $( tail -n -1 sorted_data.txt )"
